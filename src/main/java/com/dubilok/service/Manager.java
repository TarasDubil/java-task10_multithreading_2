package com.dubilok.service;

public interface Manager {
    void showTaskFirst();

    void showTaskSecond();

    void showTaskThird();

    void showFourthTask();
}
