package com.dubilok.service;

import com.dubilok.model.blocking_queue.BlockingQueueInsteadPipe;
import com.dubilok.model.ping_pong.PingPong;
import com.dubilok.model.read_write_lock.LockObjects;
import com.dubilok.model.three_methods.OneSynchronized;
import com.dubilok.model.three_methods.ThreeSynchronized;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ManagerImpl implements Manager {

    private static Logger logger = LogManager.getLogger(ManagerImpl.class);

    @Override
    public void showTaskFirst() {
        new PingPong();
    }

    @Override
    public void showTaskSecond() {
        new OneSynchronized();
        new ThreeSynchronized();
    }

    @Override
    public void showTaskThird() {
        new BlockingQueueInsteadPipe();
    }

    @Override
    public void showFourthTask() {
        new LockObjects();
    }
}
