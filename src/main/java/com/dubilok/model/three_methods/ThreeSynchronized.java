package com.dubilok.model.three_methods;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ThreeSynchronized {
    private volatile int countFirst;
    private volatile int countSecond;
    private volatile int countThird;
    private static Lock lockFirst = new ReentrantLock();
    private static Lock lockSecond = new ReentrantLock();
    private static Lock lockThird = new ReentrantLock();
    private Logger logger = LogManager.getLogger(OneSynchronized.class);

    public ThreeSynchronized() {
        logger.info("Three locks");
        Thread threadFirst = new Thread(this::incrementFirst);
        Thread threadSecond = new Thread(this::incrementSecond);
        Thread threadThird =  new Thread(this::incrementThird);
        threadFirst.start();
        threadSecond.start();
        threadThird.start();
        try {
            threadFirst.join();
            threadSecond.join();
            threadThird.join();
        } catch (InterruptedException e) {
            logger.error(e);
        }
    }

    private void incrementFirst() {
        try {
            lockFirst.lock();
            for (int i = 0; i < 1_000; i++) {
                this.countFirst++;
            }
        } finally {
            lockFirst.unlock();
        }
        logger.info(Thread.currentThread().getName() + "; countFirst = " + this.countFirst);
    }

    private void incrementSecond() {
        try {
            lockSecond.lock();
            for (int i = 0; i < 1_000; i++) {
                this.countSecond++;
            }
        } finally {
            lockSecond.unlock();
        }
        logger.info(Thread.currentThread().getName() + "; countSecond = " + this.countSecond);
    }

    private void incrementThird() {
        try {
            lockThird.lock();
            for (int i = 0; i < 1_000; i++) {
                this.countThird++;
            }
        } finally {
            lockThird.unlock();
        }
        logger.info(Thread.currentThread().getName() + "; countThird = " + this.countThird);
    }
}
