package com.dubilok.model.three_methods;

import com.dubilok.util.UtilMenu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class OneSynchronized {

    private volatile int count;
    private static Lock lock = new ReentrantLock();
    private Logger logger = LogManager.getLogger(OneSynchronized.class);

    public OneSynchronized() {
        logger.info("One lock");
        Thread threadFirst = new Thread(this::incrementFirst);
        Thread threadSecond = new Thread(this::incrementSecond);
        Thread threadThird = new Thread(this::incrementThird);
        threadFirst.start();
        try {
            Thread.sleep(10);
            threadSecond.start();
            Thread.sleep(10);
            threadThird.start();
            Thread.sleep(10);
            threadFirst.join();
            threadSecond.join();
            threadThird.join();
        } catch (InterruptedException e) {
            logger.error(e);
        }
    }

    public void setCount(int count) {
        try {
            lock.lock();
            this.count = count;
        } finally {
            lock.unlock();
        }
    }

    public int getCount() {
        try {
            lock.lock();
            return count;
        } finally {
            lock.unlock();
        }
    }

    private void incrementFirst() {
        UtilMenu.increment(lock, this, logger);
    }

    private void incrementSecond() {
        UtilMenu.increment(lock, this, logger);
    }

    private void incrementThird() {
        UtilMenu.increment(lock, this, logger);
    }
}
