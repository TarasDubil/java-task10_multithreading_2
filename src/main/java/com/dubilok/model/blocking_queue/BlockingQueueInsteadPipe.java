package com.dubilok.model.blocking_queue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ResourceBundle;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

public class BlockingQueueInsteadPipe {

    private static BlockingQueue<Character> queue = new PriorityBlockingQueue<>();
    private static ResourceBundle constant = ResourceBundle.getBundle("constant");
    private static Logger logger = LogManager.getLogger(BlockingQueueInsteadPipe.class);
    private static volatile String data;
    private static int size;

    {
        data = constant.getString("data");
    }

    public BlockingQueueInsteadPipe() {
        start();
    }

    public void start() {
        new Thread(() -> {
            char[] array = data.toCharArray();
            logger.info("Written: " + data);
            for (Character c : array) {
                try {
                    Thread.sleep(100);
                    queue.add(c);
                } catch (InterruptedException e) {
                    logger.error(e);
                }
            }
        }).start();
        new Thread(() -> {
            logger.info("Read: ");
            try {
                while (queue.isEmpty()) {
                    Thread.sleep(100);
                    Character character = queue.take();
                    size++;
                    logger.info(character);
                    if (size == data.length()) {
                        break;
                    }
                }
            } catch (InterruptedException e) {
                logger.error(e);
            }
        }).start();
    }
}
