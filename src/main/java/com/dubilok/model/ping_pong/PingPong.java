package com.dubilok.model.ping_pong;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class PingPong {

    private static Lock lock = new ReentrantLock();
    private static Condition condition = lock.newCondition();
    private static Logger logger = LogManager.getLogger(PingPong.class);

    public PingPong() {
        start();
    }

    public static void start() {
        Ping ping = new Ping();
        Pong pong = new Pong();
        ping.start();
        pong.start();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            logger.error(e);
        }
        ping.interrupt();
        pong.interrupt();
    }

    static class Ping extends Thread {
        @Override
        public void run() {
            try {
                lock.lock();
                while (!Thread.interrupted()) {
                    try {
                        condition.await();
                        logger.info("Ping");
                        condition.signal();
                    } catch (InterruptedException e) {
                        logger.error(e);
                    }
                }
            } finally {
                lock.unlock();
            }
        }
    }

    static class Pong extends Thread {
        @Override
        public void run() {
            try {
                lock.lock();
                while (!Thread.interrupted()) {
                    condition.signal();
                    logger.info("Pong");
                    try {
                        condition.await();
                    } catch (InterruptedException e) {
                        logger.error(e);
                    }
                }
            } finally {
                lock.unlock();
            }
        }
    }
}
