package com.dubilok.model.read_write_lock;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ResourceBundle;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import static java.lang.Thread.sleep;

public class LockObjects {
    private String obj = "";
    private ResourceBundle resourceBundle = ResourceBundle.getBundle("constant");
    private static Logger logger = LogManager.getLogger(LockObjects.class);
    private final static ReadWriteLock MY_LOCK_FIRST = new ReentrantReadWriteLock();
    private final static ReadWriteLock MY_LOCK_SECOND = new ReentrantReadWriteLock();

    public LockObjects() {
        showSynchronizedByMyLocker();
    }

    private void changeToFirstMyLocker(String string) {
        MY_LOCK_FIRST.writeLock().lock();
        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        string = resourceBundle.getString("first");
        logger.info("String obj = " + string);
        MY_LOCK_FIRST.writeLock().unlock();
    }

    private void changeToSecondMyLocker(String string) {
        MY_LOCK_FIRST.writeLock().lock();
        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        string = resourceBundle.getString("second");
        logger.info("String obj = " + string);
        MY_LOCK_FIRST.writeLock().unlock();
    }

    private void changeToThirdMyLocker(String string) {
        MY_LOCK_SECOND.readLock().lock();
        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        string = resourceBundle.getString("third");
        logger.info("String obj = " + string);
        MY_LOCK_SECOND.readLock().unlock();
    }


    public void showSynchronizedByMyLocker() {
        new Thread(() -> changeToFirstMyLocker(obj)).start();
        new Thread(() -> changeToSecondMyLocker(obj)).start();
        new Thread(() -> changeToThirdMyLocker(obj)).start();
        try {
            sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
