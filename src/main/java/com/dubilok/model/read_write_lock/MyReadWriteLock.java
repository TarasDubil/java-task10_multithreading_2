package com.dubilok.model.read_write_lock;

public class MyReadWriteLock {

    private int readLockCount;
    private int writeLockCount;
    private final MyReadWriteLock.ReadLock readLock;
    private final MyReadWriteLock.WriteLock writeLock;

    public MyReadWriteLock() {
        readLock = new ReadLock();
        writeLock = new WriteLock();
    }

    public class ReadLock {
        synchronized void lock() {
            if (writeLockCount == 0) {
                readLockCount++;
            } else {
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        synchronized void unlock() {
            readLockCount--;
            if (readLockCount == 0) {
                notify();
            }
        }
    }

    public class WriteLock {
        synchronized void lock() {
            if ((writeLockCount == 0) && (readLockCount == 0)) {
                writeLockCount++;
            } else {
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        synchronized void unlock() {
            writeLockCount--;
            notify();
        }
    }

    public ReadLock readLock() {
        return readLock;
    }

    public WriteLock writeLock() {
        return writeLock;
    }
}
