package com.dubilok.controller;

public interface Controller {
    void firstTask();

    void secondTask();

    void thirdTask();

    void fourthTask();
}
