package com.dubilok.view;

import com.dubilok.controller.Controller;
import com.dubilok.controller.ControllerImpl;
import com.dubilok.util.UtilMenu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class ViewImpl implements View {

    private Controller controller;
    private Map<String, String> menu;
    private static ResourceBundle constant = ResourceBundle.getBundle("constant");
    private Map<String, Printable> methodsMenu;
    private Logger logger = LogManager.getLogger(ViewImpl.class);
    private static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    public ViewImpl() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        start();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
    }

    public void start() {
        menu.put("1", constant.getString("1"));
        menu.put("2", constant.getString("2"));
        menu.put("3", constant.getString("3"));
        menu.put("4", constant.getString("4"));
        menu.put("q", constant.getString("q"));
    }

    private void pressButton1() {
        controller.firstTask();
        try {
            Thread.sleep(Long.parseLong(constant.getString("sleep2")));
        } catch (InterruptedException e) {
            logger.error(e);
        }
    }

    private void pressButton2() {
        controller.secondTask();
    }

    private void pressButton3() {
        controller.thirdTask();
        try {
            Thread.sleep(Long.parseLong(constant.getString("sleep1")));
        } catch (InterruptedException e) {
            logger.error(e);
        }
    }

    private void pressButton4() {
        controller.fourthTask();
    }

    @Override
    public void show() {
        UtilMenu.show(bufferedReader, menu, methodsMenu);
    }
}
